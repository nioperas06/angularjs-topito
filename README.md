# angularjs-topito

> Warning: Make sure you're using the latest version of Node.js and NPM

## Quick start

```bash
# clone our repo
$ git clone https://nioperas06@bitbucket.org/nioperas06/angularjs-topito.git

# change directory to your app
$ cd angularjs-topito

# install the dependencies with npm
$ npm install

# start the server
$ npm start
```

go to <http://localhost:8080> in your browser.

# Build files

- single run: `npm run build`

copy dist/ folder on a server
