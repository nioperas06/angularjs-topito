import angular from 'angular';

import '../style/app.css';
import { Mood } from './mood.ts';

let app = () => {
  return {
    template: require('./app.html'),
    controller: 'AppCtrl',
    controllerAs: 'ctrl'
  }
};

class AppCtrl {
  constructor() {
    this.moods = [
      new Mood(
        'Angry',
        `https://emojipedia-us.s3.amazonaws.com/thumbs/72/facebook/111/pouting-face_1f621.png`
      ),
      new Mood(
        'Sad',
        `https://emojipedia-us.s3.amazonaws.com/thumbs/72/facebook/111/disappointed-but-relieved-face_1f625.png`
      ),
      new Mood(
        'Average',
        `https://emojipedia-us.s3.amazonaws.com/thumbs/72/facebook/111/neutral-face_1f610.png`
      ),
      new Mood(
        'Happy',
        `https://emojipedia-us.s3.amazonaws.com/thumbs/72/facebook/111/slightly-smiling-face_1f642.png`
      ),
      new Mood(
        'Very Happy',
        `https://emojipedia-us.s3.amazonaws.com/thumbs/72/facebook/111/hugging-face_1f917.png`
      )
    ];
  }
  press(mood) {
    mood.increment();
  }
}

const MODULE_NAME = 'app';

angular.module(MODULE_NAME, [])
  .directive('app', app)
  .controller('AppCtrl', AppCtrl);

export default MODULE_NAME;
