export class Mood{
  constructor(
    public name: string,
    public url: string,
    public counter: number = 0
  ){}

  increment(){
    this.counter ++;
  }
}
